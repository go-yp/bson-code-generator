module gitlab.com/go-yp/bson-code-generator

go 1.13

require (
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-mgo/mgo v0.0.0-20180705113738-7446a0344b78 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
)

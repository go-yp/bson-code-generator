package tests

import (
	"bytes"
	"encoding/json"
	"gitlab.com/go-yp/bson-code-generator/models"
	"testing"
)

func TestUserEncodingJSON(t *testing.T) {
	var user = models.User{
		ID:   1,
		Name: "Alex",
	}

	var actual, marshalErr = json.Marshal(user)
	if marshalErr != nil {
		t.Fatal(marshalErr)
	}

	const expect = `{"id":1,"name":"Alex"}`
	if !bytes.Equal([]byte(expect), actual) {
		t.Errorf("expect %s, got %s", expect, actual)
	}
}

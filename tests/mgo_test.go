package tests

import (
	"bytes"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/go-yp/bson-code-generator/models"
	"testing"
)

func TestUserEncodingBSON(t *testing.T) {
	var user = models.User{
		ID:   1,
		Name: "Alex",
	}

	var actual, marshalErr = bson.Marshal(user)
	if marshalErr != nil {
		t.Fatal(marshalErr)
	}

	// http://bsonspec.org/spec.html
	var expect = []byte{
		28, 0, 0, 0, // 28 is length 4 bytes
		16,          // "\x10" e_name int32	32-bit integer by http://bsonspec.org/spec.html
		105, 100, 0, // String.fromCharCode(105, 100) = "id", 0 is delimeter
		1, 0, 0, 0, // ID = 1 by 4 bytes
		2,                    // "\x02" e_name string	UTF-8 string by http://bsonspec.org/spec.html
		110, 97, 109, 101, 0, // String.fromCharCode(110, 97, 109, 101) = "name", 0 is delimeter
		5, 0, 0, 0, // 5 is length by int32 = "Alex".length + 1
		65, 108, 101, 120, 0, // String.fromCharCode(65, 108, 101, 120) = "Alex", 0 is delimeter
		0, // 0 is delimeter
	}
	if !bytes.Equal(expect, actual) {
		t.Errorf("expect %s, got %s", expect, actual)
	}
}
